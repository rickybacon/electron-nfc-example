const { NFC } = require('nfc-pcsc');
const nfc = new NFC(); // optionally you can pass logger
// const StringDecoder = require('string_decoder').StringDecoder;  
// const decoder = new StringDecoder('utf8');
const videoNames = [
    'CYNTHIA_Subtitled.mp4', 
    'EDINA_Subtitled.mp4', 
    'HOPE_Subtitled.mp4', 
    'LUWIZA_Subtitled.mp4', 
    'EDNA_Subtitled.mp4'
];
let randomVideos= videoNames;
const videoElement = document.getElementsByTagName('video')[0];
const videoSource = document.createElement('source');

// let's set up a very simple state machine
const IDLE = 0;         // when our experience is idling
const CARD_VIDEO = 1;   // when we are playing the video linked to the card
const RAND_VIDEO_1 = 2; // when we are playing the first randomized video
const RAND_VIDEO_2 = 3; // when we are playing the second randomized video

let state = IDLE;

init();

function init() {
    console.log('nfclib.js loaded');
    playIdleVideo();
}

function playIdleVideo() {
    playVideo(`file://${__dirname}/videos/Loading_screen.mp4`);
}

function playRandomVideo() {
    const idx = Math.floor(Math.random() * randomVideos.length);
    const randomVideoSrc = randomVideos[idx];
    console.log(randomVideos);
    randomVideos.splice(idx, 1); // remove the video we just played
    console.log(randomVideos);
    const videoSourceURL = `file://${__dirname}/videos/randomVideoSrc`;
    console.log(videoSourceURL);
    playVideo(videoSourceURL);
}

function playVideo(videoSrcUrl) {
    videoSource.setAttribute('src', videoSrcUrl);    
    videoElement.appendChild(videoSource);
    videoElement.load();
    videoElement.play();
}

function resetRandomVideos(currentVideo) {
    // remove the card video from the video stack
    randomVideos = videoNames.filter(function (value, index, arr) {
        return value !== videoNames[currentVideo];
    });
    console.log(`videoNames`, videoNames);
    console.log(`randomVideos`, randomVideos);
}

function updateState(data = null) {
    console.log(`state`, state);
    switch(state) {
        case IDLE: // state moves from idle to the swiped card
            resetRandomVideos(data);
            const videoSrc = videoNames[data];
            const videoSourceURL = `file://${__dirname}/videos/${videoSrc}`;
            playVideo(videoSourceURL);
            state = CARD_VIDEO;
            break;
        case CARD_VIDEO: // state moves from swiped card to random video 1
            playRandomVideo();
            state = RAND_VIDEO_1;
            break;
        case RAND_VIDEO_1: // state moves from swiped card to random video 2
            playRandomVideo();
            state = RAND_VIDEO_2;
            break;
        case RAND_VIDEO_2:  // state moves from random video 2 to idle
            playIdleVideo();
            state = IDLE;
            break;
        default:
    }
    console.log(`state`, state);
}

nfc.on('reader', reader => {
    reader.on('card', async card => {
        console.log(`${reader.reader.name} card inserted`, card);
        
        try {
            const data = await reader.read(4, 4);
            console.log(`data read`, data);
            const cardID = data.readInt16BE(0);
            console.log(`data converted`, cardID);

            try {
                state = IDLE; // catch the case where a non-idle video is playing and a card is swiped
                updateState(cardID);
            } catch (err) {
                console.error(err);
            }
        } catch (err) {
            console.error(`error when reading data`, err);
        }

        reader.autoProcessing = false;
    });
});

videoElement.onended = e => {
    switch(state) {
        case IDLE:
            playIdleVideo();
            break;
        default:
            updateState();
    }
}

// set up some key press functions to enable experience with keyboard only
document.onkeyup = e => {
    state = IDLE; // reset state
    switch(e.which) {
        case 32: //spacebar
            // restart experience
            playIdleVideo();
            break;
        case 48: // 0 
            updateState(0);
            break;
        case 49: // 1
            updateState(1);
            break;
        case 50: // 2
            updateState(2);
            break;
        case 51: // 3
            updateState(3);
            break;
        case 52: // 4
            updateState(4);
            break;
        case 53: // 5
            updateState(5);
            break;
        case 54: // 6
            updateState(6);
            break;
        case 55: // 7 
            updateState(7);
            break;
        case 56: // 8
            updateState(8);
            break;
        case 57: // 9
            updateState(9);
            break;
    }
}